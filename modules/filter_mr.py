import fnmatch
import re

class ParserError(Exception):
    pass


class FilterParser:
    keywords = {
        'draft': 'work_in_progress',
        'wip': 'work_in_progress',
    }

    re_whitespace = re.compile(r'[ \t\n]+')
    token_classes = {
        'label': re.compile(r'[a-zA-Z][a-zA-Z0-9_:]*'),
        'label_wildcard': re.compile(r'([a-zA-Z][a-zA-Z0-9_:]*)?((\[!?[a-zA-Z0-9_:-]+\]|\*|\?)[a-zA-Z0-9_:]*)+'),
        'group': re.compile(r'\('),
        'group_end': re.compile(r'\)'),
        'op_and': re.compile(r'[&+]'),
        'op_or': re.compile(r'\|'),
        'op_not': re.compile(r'[~-]'),
        'op_and_not': re.compile(r'-'),
        'eof': re.compile(r'$'),
    }

    # parse rules:
    # expression := conjunction OP_OR expression |
    #               conjunction expression |
    #               conjunction
    # conjunction := negation OP_AND_NOT conjunction |
    #                negation OP_AND conjunction |
    #                negation
    # negation := OP_NOT negation |
    #             primitive
    # primitive := GROUP expression GROUP_END |
    #              LABEL_WILDCARD |
    #              LABEL

    def __init__(self, expression):
        self.expression = expression
        self.pos = 0
        if self.lookahead('eof') is None:
            self.graph = self.parse_expression()
        else:
            self.graph = FilterTrue()

    def __call__(self, mr):
        return self.graph(mr)

    def error(self, msg):
        raise ParserError('{}\n{}\n{}^'.format(msg, self.expression, ' ' * self.pos))

    def get_token(self, cls, lookahead=False):
        m = self.re_whitespace.match(self.expression, self.pos)
        if m:
            self.pos += len(m.group())
        m = self.token_classes[cls].match(self.expression, self.pos)
        if m:
            result = m.group()
            if not lookahead:
                self.pos += len(result)
            return result
        return None

    def lookahead(self, cls):
        return self.get_token(cls, lookahead=True)

    def parse_expression(self):
        left = self.parse_conjunction()
        if self.lookahead('eof') is not None or self.lookahead('group_end'):
            return left
        self.get_token('op_or')
        right = self.parse_expression()
        return FilterOr(left, right)

    def parse_conjunction(self):
        left = self.parse_negation()
        if self.get_token('op_and_not'):
            op = FilterAndNot
        elif self.get_token('op_and'):
            op = FilterAnd
        else:
            return left
        right = self.parse_conjunction()
        return op(left, right)

    def parse_negation(self):
        if not self.get_token('op_not'):
            return self.parse_primitive()
        return FilterNot(self.parse_negation())

    def parse_primitive(self):
        if self.get_token('group'):
            result = self.parse_expression()
            if not self.get_token('group_end'):
                self.error('missing )')
            return result
        label = self.get_token('label_wildcard')
        if label:
            return FilterLabelWildcard(label)
        label = self.get_token('label')
        if label:
            lower_label = label.lower()
            if lower_label in self.keywords:
                return FilterKeyword(self.keywords[lower_label])
            return FilterLabel(label)
        self.error('unexpected character')


class FilterNode:
    def __call__(self, mr):
        raise NotImplementedError

class FilterTrue(FilterNode):
    def __call__(self, mr):
        return True

class FilterValue(FilterNode):
    def __init__(self, value):
        self.value = value

class FilterBinary(FilterNode):
    def __init__(self, left, right):
        self.left = left
        self.right = right

class FilterUnary(FilterNode):
    def __init__(self, op):
        self.op = op

class FilterLabel(FilterValue):
    def __call__(self, mr):
        return self.value in mr['labels']

class FilterLabelWildcard(FilterValue):
    def __call__(self, mr):
        return any(fnmatch.fnmatchcase(label, self.value) for label in mr['labels'])

class FilterKeyword(FilterValue):
    def __call__(self, mr):
        return bool(mr[self.value])

class FilterAnd(FilterBinary):
    def __call__(self, mr):
        return self.left(mr) and self.right(mr)

class FilterAndNot(FilterBinary):
    def __call__(self, mr):
        return self.left(mr) and not self.right(mr)

class FilterOr(FilterBinary):
    def __call__(self, mr):
        return self.left(mr) or self.right(mr)

class FilterNot(FilterUnary):
    def __call__(self, mr):
        return not self.op(mr)


__test__ = {}
__test__['FilterParser'] = """
>>> f = FilterParser('x')
>>> f({ 'labels': ('a', 'b') })
False
>>> f({ 'labels': ('x', 'y') })
True
>>> f = FilterParser('-x')
>>> f({ 'labels': ('a', 'b') })
True
>>> f({ 'labels': ('x', 'y') })
False
>>> f = FilterParser('x::1 | x::2')
>>> f({ 'labels': ('x', 'y') })
False
>>> f({ 'labels': ('x', 'y', 'x::1') })
True
>>> f({ 'labels': ('x', 'y', 'x::2') })
True
>>> f = FilterParser('x::1 & x::2')
>>> f({ 'labels': ('x::1',) })
False
>>> f({ 'labels': ('x::1', 'x::2') })
True
>>> f = FilterParser('a | b & c')
>>> f({ 'labels': ('a', 'x') })
True
>>> f({ 'labels': ('b', 'x') })
False
>>> f({ 'labels': ('b', 'x', 'c') })
True
>>> f = FilterParser('(a b) + c')
>>> f({ 'labels': ('a', 'x') })
False
>>> f({ 'labels': ('b', 'x', 'c') })
True
>>> f = FilterParser('(a b) - c')
>>> f({ 'labels': ('a', 'x') })
True
>>> f({ 'labels': ('b', 'x', 'c') })
False
>>> f({ 'labels': ('x',) })
False
>>> f = FilterParser('-a')
>>> f({ 'labels': ('a', 'x') })
False
>>> f({ 'labels': ('b', 'x') })
True
>>> f = FilterParser('-a-b')
>>> f({ 'labels': ('a', 'x') })
False
>>> f({ 'labels': ('b', 'x') })
False
>>> f({ 'labels': ('c', 'x') })
True
>>> f = FilterParser('~(a | b) & (c)')
>>> f({ 'labels': ('a', 'c') })
False
>>> f({ 'labels': ('b',) })
False
>>> f({ 'labels': ('c',) })
True
>>> f = FilterParser('a*b')
>>> f({ 'labels': ('ac', 'cb') })
False
>>> f({ 'labels': ('acb',) })
True
>>> f({ 'labels': ('acbc',) })
False
>>> f = FilterParser('a::b*')
>>> f({ 'labels': ('a::c', 'a::d') })
False
>>> f({ 'labels': ('a::b',) })
True
>>> f({ 'labels': ('a::b::c',) })
True
>>> f = FilterParser('*')
>>> f({ 'labels': ('x') })
True
>>> f = FilterParser('a[x-z] b?c')
>>> f({ 'labels': ('aw', 'bc') })
False
>>> f({ 'labels': ('ay', 'bc') })
True
>>> f({ 'labels': ('aw', 'bcc') })
True
>>> f = FilterParser('draft')
>>> f({ 'labels': (), 'work_in_progress': False })
False
>>> f({ 'labels': (), 'work_in_progress': True })
True
>>> f = FilterParser('a b (c')
Traceback (most recent call last):
  ...
modules.filter_mr.ParserError: missing )
a b (c
      ^
>>> f = FilterParser('a.b')
Traceback (most recent call last):
  ...
modules.filter_mr.ParserError: unexpected character
a.b
 ^
>>> f = FilterParser('1*')
Traceback (most recent call last):
  ...
modules.filter_mr.ParserError: unexpected character
1*
^
>>> f = FilterParser('')
>>> f(None)
True
"""
