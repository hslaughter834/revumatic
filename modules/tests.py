import doctest
import sys

sys.path.insert(0, '..')

from modules import (   # noqa: E402
    filter_mr,
)

doctest.testmod(filter_mr)
