import tuimatic

from modules import (
    formatters,
    gitlab,
    mr_activity,
    mr_ui,
    patch,
    utils,
)


class MRCommitGetter:
    def __init__(self, repo, data, override_base_sha, number, count):
        self.repo = repo
        self.data = data
        self.base_sha = override_base_sha
        self.number = number
        self.count = count
        self.created_at = gitlab.parse_datetime(data['created_at'])
        self.commits = None
        self.parsed_diff = None

    def set_shas(self, project, mr, activity):
        if self.number == self.count:
            self.base_sha = mr['diff_refs']['base_sha']
            self.head_sha = mr['diff_refs']['head_sha']
            self.merge_base_sha = None

            new_base = self.repo.pkg.get_base_sha(mr)
            if new_base:
                try:
                    new_base = self.repo.full_sha(new_base)
                    if new_base != self.base_sha:
                        self.merge_base_sha = self.base_sha
                        self.base_sha = new_base
                except KeyError:
                    pass
            return

        if not self.base_sha:
            self.base_sha = self.data['base_commit_sha']
        self.head_sha = self.data['head_commit_sha']
        self.merge_base_sha = None
        new_base = self.repo.pkg.get_historical_base_sha(self.number, activity)
        if not new_base:
            return
        if len(new_base) == 40:
            self.base_sha = new_base
            return
        try:
            self.base_sha = self.repo.full_sha(new_base)
            return
        except KeyError:
            pass
        # The commit is not available locally. We need to query the API
        # for the full commit id. We can fetch an arbitrary commit id
        # from the remote only if we have its full sha.
        self.base_sha = project.commit(new_base)['id']

    def get_commit_range(self, base, head, output_callback=None):
        return [(c, self.repo.pkg.get_upstream(c))
                for c in self.repo.get_commits(output_callback, base, head)]

    def get_commits(self, output_callback=None):
        if self.commits is None:
            self.commits = self.get_commit_range(self.base_sha, self.head_sha,
                                                 output_callback)
        return self.commits

    def get_additional_upstream_shas(self, output_callback=None):
        """Get commits between the target branch and our presented base sha,
        i.e. the not shown dependencies. Returns a set of upstream shas."""
        result = set()
        if self.merge_base_sha:
            for _, upstream in self.get_commit_range(self.merge_base_sha, self.base_sha,
                                                     output_callback):
                if upstream:
                    result.update(set(upstream))
        return result

    def get_parsed_diff(self):
        if self.parsed_diff is None:
            self.parsed_diff = self.repo.get_parsed_diff_range(self.base_sha, self.head_sha)
        return self.parsed_diff


class DispCommit:
    def __init__(self, name):
        self.name = name
        self.type = None
        self.widgets = []
        self.content_map = {}
        self.sep_map = {}
        self.marks = []
        self.comment_count = 0
        self.file_list = set()
        self.list_item_widget = None
        self.ref_commit = None
        # backend_sha is the SHA used to assign comments in the backend
        # (i.e. GitLab). If it is set, the commit can be commented on.
        self.backend_sha = None

    def set_type_backport(self, index, git_commit, upstream, comparison):
        self.type = 'backport'
        self.index = index
        self.git_commit = git_commit
        self.sha = str(self.git_commit.id)
        self.backend_sha = self.sha
        self.upstream = upstream
        self.comparison = comparison

    def set_type_removed(self, git_commit):
        self.type = 'removed'
        self.git_commit = git_commit
        self.sha = str(self.git_commit.id)

    def set_type_cover(self, comparison):
        self.type = 'cover'
        self.index = 0
        self.sha = '-' * 40
        self.comparison = comparison

    def set_type_suggested(self, sha, ref_commit, subtype):
        """Recognized subtypes: 'missing', 'maybe', 'series'."""
        self.type = 'suggested'
        self.category = subtype
        self.sha = sha
        self.ref_commit = ref_commit

    def build_widgets(self, content_defs, **data):
        """Creates a TextBox for each entry in content_defs and fills it
        with text widgets containing the passed data according to each
        TextBox's content definition. For the meaning of the data fields,
        see mr_layout.LayoutBase.create_content_widgets. The content of the
        data fields is a markup or a text widget."""
        sep = {
            'annot': '',
            'header': '',
            'stat': '---',
            'diff': '',
            'diff_all': '',
        }
        classes = {
            'diff': mr_ui.DiffPile,
            'diff_all': mr_ui.DiffViewer,
            'stat': mr_ui.DiffstatViewer,
        }
        for keys in content_defs:
            widgets = []
            for key in keys:
                val = data.get(key)
                if not val:
                    continue
                if widgets:
                    if key not in self.sep_map:
                        if key not in sep:
                            sep_widget = None
                        elif len(sep[key]) == 1:
                            sep_widget = tuimatic.TextSeparator(sep[key])
                        else:
                            sep_widget = tuimatic.Viewer(sep[key])
                        self.sep_map[key] = sep_widget
                    else:
                        sep_widget = self.sep_map[key]
                    if sep_widget:
                        widgets.append(sep_widget)
                if key not in self.content_map:
                    if not isinstance(val, tuimatic.TextBase):
                        cls = classes.get(key, tuimatic.Viewer)
                        val = cls(val)
                    self.content_map[key] = val
                widgets.append(self.content_map[key])
            self.widgets.append(tuimatic.TextBox(widgets))

    def get_widget(self, content_type):
        """Returns the widget for the given content_type or None."""
        return self.content_map.get(content_type)

    def toggle_widget(self, content_type):
        """Toggles visibility of the widget for the given content_type."""
        w = self.content_map.get(content_type)
        if not w:
            return
        sep = self.sep_map.get(content_type)
        for box in self.widgets:
            try:
                state = not box.get_enabled(w)
            except ValueError:
                continue
            box.set_enabled(w, state)
            if sep:
                try:
                    box.set_enabled(sep, state)
                except ValueError:
                    pass

    def show_threads(self, enabled):
        widget = self.get_widget('diff')
        if widget:
            widget.show_threads(enabled)

    def set_marks(self, marks):
        self.marks = marks

    def get_mark(self, index):
        if index >= len(self.marks):
            return ' '
        return self.marks[index]

    def set_mark(self, index, mark):
        if index >= len(self.marks):
            self.marks.extend(' ' * (index - len(self.marks) + 1))
        self.marks[index] = mark

    def set_list_item_widget(self, widget):
        """Associates a widget (typically SelectableIcon) in the commit list
        to this commit. This allows quickly referencing the coresponding
        list entry from the commit."""
        self.list_item_widget = widget

    def set_file_list(self, file_list):
        self.file_list = set(file_list)

    def touches_file(self, path):
        return path in self.file_list

    def touches_file_regex(self, include, exclude):
        if include is None:
            return False
        for f in self.file_list:
            if include.search(f):
                if exclude is None or not exclude.search(f):
                    return True
        return False

    def is_type_with_index(self):
        return self.type in ('backport', 'cover')

    def is_series(self):
        return self.type == 'suggested' and self.category == 'series'

    def is_suggested_error(self):
        return self.type == 'suggested' and self.category in ('missing', 'fixing')

    def has_comparison(self):
        return self.is_type_with_index() and self.comparison

    def get_sha(self, length=40):
        return self.sha[:length]


class BaseDispCommitList(list):
    NUM_MARKS = 0

    def __init__(self, output_callback, commit_getter, repo, repos, *args):
        super().__init__()
        self.num_marks = self.NUM_MARKS + repo.pkg.marks
        self.commit_getter = commit_getter
        self.repo = repo
        self.repos = repos
        self.pre_parse(output_callback)
        self.parse(*args)

    def pre_parse(self, output_callback):
        return

    def get_peer_cover(self):
        return None

    def get_peer_diff(self, i, c, upstream):
        return None, None

    def update_annot_marks(self, annot, marks, upstream, c, diff):
        return

    def get_compared_titles(self):
        # use the default titles from formatters.CommitFormatter:
        return None

    def add_more(self, app, mr, content_defs):
        return

    def get_parsed_diff(self, commit):
        return self.repo.get_parsed_diff(str(commit.oid))

    def new_disp_commit(self, name):
        return DispCommit(name)

    def parse(self, app, mr, cover_annot, cover_disc, content_defs):
        popup = app.wait_log_popup('Loading commits...')

        compared_titles = self.get_compared_titles()

        repo_data = self.commit_getter.get_commits(popup.append)
        diff = self.commit_getter.get_parsed_diff()
        compared_sha = None
        peer_diff = self.get_peer_cover()
        if peer_diff:
            diff.compare(peer_diff)
            compared_sha = (diff.identifier, peer_diff.identifier)
        cf = formatters.CommitFormatter(mr['description'], diff, compared_sha, compared_titles)

        self.additional_upstream_list = self.commit_getter.get_additional_upstream_shas(popup.append)

        popup.stop()
        popup = app.progress_popup(len(repo_data))

        disp = self.new_disp_commit(utils.oneline(mr['title']))
        disp.set_type_cover(cf.format_compared())
        disp.build_widgets(content_defs,
                           annot=cover_annot, header=cf.format_header(),
                           stat=cf.get_stat(), diff_all=cf.format_diff()[0], disc=cover_disc)
        marks = []
        self.update_annot_marks(None, marks, None, None, diff)
        while len(marks) < self.num_marks:
            marks.append(' ')
        disp.set_marks(marks)
        self.append(disp)

        self.file_list = cf.get_file_list()

        # format commits
        i = -1
        for i, (c, upstream) in enumerate(repo_data):
            app.progress_update(popup, i + 1)

            diff = self.get_parsed_diff(c)
            peer_sha, peer_diff = self.get_peer_diff(i, c, upstream)
            if peer_diff:
                diff.compare(patch.PatchParser(peer_sha[:12], peer_diff).parse())

            cf = formatters.CommitFormatter(c, diff, peer_sha, compared_titles)
            disp = self.new_disp_commit(cf.name())
            disp.set_type_backport(i + 1, c, peer_sha, cf.format_compared())

            annot = formatters.AnnotationFormatter('desc')
            marks = []
            self.update_annot_marks(annot, marks, upstream, c, diff)
            # the last mark is always the comment mark
            marks.append(' ')

            disp.build_widgets(content_defs,
                               annot=annot.format(), header=cf.format_header(),
                               stat=cf.get_stat(), diff=cf.format_diff())
            handler = mr_activity.CommitCommentHandler(disp, self.repo)
            diff_widget = disp.get_widget('diff')
            if diff_widget:
                diff_widget.comment_handler = handler
            disp.set_marks(marks)
            disp.set_file_list(cf.get_file_list())
            self.append(disp)
        self.count = i + 1

        popup.stop()

        self.add_more(app, mr, content_defs)


class DispCommitList(BaseDispCommitList):
    NUM_MARKS = 2

    def pre_parse(self, output_callback):
        self.marks_help = { k: (d, c) for k, d, c in self.repo.pkg.get_marks_help() }
        self.upstream_list = {}

    def get_upstream_diff(self, oid):
        diff = self.repos.get_diff(oid)
        if not diff:
            diff = self.repo.pkg.get_diff(oid)
        return diff

    def get_peer_diff(self, i, c, upstream):
        if not upstream:
            return None, None
        for u in upstream:
            self.upstream_list[u] = { 'index': i + 1 }
        upstream_diff = self.get_upstream_diff(upstream[0])
        if not upstream_diff:
            return None, None
        return upstream[0], upstream_diff

    def update_annot_marks(self, annot, marks, upstream, c, diff):
        if not annot:
            return

        mark1 = ' '
        if upstream:
            if diff.was_compared():
                if diff.modified_hunks + diff.extra_hunks + diff.missing_hunks > 0:
                    mark1 = 'C'
                    annot.add(('desc-label-err', 'Code difference'), label=True)
                elif diff.context_diff:
                    mark1 = '·'
                    annot.add(('desc-label-warn', 'Context difference'), label=True)
            else:
                mark1 = '?'
                annot.add(('desc-label-warn', 'Unknown upstream'), label=True)
        elif upstream is None:
            mark1 = 'R'
            annot.add(('desc-label-err', 'RHEL only'), label=True)
        else:
            mark1 = '!'
            annot.add(('desc-label-err', 'No upstream reference'), label=True)
        marks.append(mark1)

        for m in self.repo.pkg.get_marks(c, diff):
            marks.append(m)
            if m != ' ':
                annot.add(('desc-label-{}'.format(self.marks_help[m][1]),
                          self.marks_help[m][0]), label=True)

    def _get_fixes(self, mr):
        return self.repo.pkg.get_fixes(list(self.upstream_list.keys()), mr, self.repo)

    def _exclude_fixes(self, mr):
        return self.repo.pkg.exclude_fixes([disp.git_commit for disp in self
                                                            if disp.type == 'backport'],
                                           mr)

    def add_more(self, app, mr, content_defs):
        self.has_series = False
        popup = app.wait_popup('Checking for fixes...')

        # get and format missing fixes
        fixes = self._get_fixes(mr)
        omit = self._exclude_fixes(mr)
        for upstream, fix, subj, suggestion_type in fixes:
            skip = False
            for sha in omit:
                if fix.startswith(sha):
                    # in the exclude list, ignore
                    skip = True
                    break
            if skip:
                continue
            if fix in self.upstream_list or fix in self.additional_upstream_list:
                # already part of the MR
                continue
            if 'fixes' not in self.upstream_list[upstream]:
                self.upstream_list[upstream]['fixes'] = []
            self.upstream_list[upstream]['fixes'].append((fix, subj, suggestion_type))
        updates = [(u['index'], u['fixes']) for u in self.upstream_list.values()
                                            if 'fixes' in u]
        updates.sort(key=lambda x: x[0], reverse=True)
        for u in updates:
            for f in reversed(u[1]):
                annot = formatters.AnnotationFormatter('desc')
                if f[2] == 'maybe':
                    annot.add(('desc-label-err', 'Maybe a commit to add'), label=True)
                elif f[2] == 'missing':
                    annot.add(('desc-label-err', 'Missing fix'), label=True)
                elif f[2] == 'fixing':
                    annot.add(('desc-label-err', 'Missing the commit being fixed'), label=True)
                elif f[2] == 'series':
                    annot.add(('desc-label-err', 'A commit from the same upstream series'),
                              label=True)
                    self.has_series = True
                upstream_diff = self.get_upstream_diff(f[0])
                diff = patch.PatchParser(f[0], upstream_diff).parse()
                cf = formatters.CommitFormatter(None, diff)
                disp = self.new_disp_commit(f[1])
                disp.set_type_suggested(f[0], self[u[0]], f[2])
                disp.build_widgets(content_defs,
                                   annot=annot.format(), header=cf.format_header(),
                                   stat=cf.get_stat(), diff=cf.format_diff())
                self.insert(u[0] + 1, disp)

        popup.stop()

        # cleanup, free memory
        self.upstream_list = None


class DispCommitVersionList(BaseDispCommitList):
    NUM_MARKS = 1

    def __init__(self, output_callback, commit_getters, *args):
        self.commit_getter_prev = commit_getters[0]
        super().__init__(output_callback, commit_getters[1], *args)

    def pre_parse(self, output_callback):
        self.prev_used = [0] * len(self.commit_getter_prev.get_commits(output_callback))

    def get_peer_cover(self):
        return self.commit_getter_prev.get_parsed_diff()

    def get_peer_diff(self, i, c, upstream):
        if upstream:
            upstream = upstream[0]
        name = formatters.commit_name(c)
        for prev_i, (prev_c, prev_upstream) in enumerate(self.commit_getter_prev.get_commits()):
            if prev_upstream:
                prev_upstream = prev_upstream[0]
            if self.prev_used[prev_i] or prev_upstream != upstream:
                continue
            if not upstream and name != formatters.commit_name(prev_c):
                continue
            self.prev_used[prev_i] = i + 1
            return str(prev_c.oid), self.repo.get_diff(prev_c)
        return None, None

    def update_annot_marks(self, annot, marks, upstream, c, diff):
        mark1 = ' '
        if diff.was_compared():
            if diff.modified_hunks + diff.extra_hunks + diff.missing_hunks > 0:
                mark1 = 'C'
                if annot:
                    annot.add(('desc-label-err', 'Code difference between versions'),
                              label=True)
            elif diff.context_diff:
                mark1 = '·'
                if annot:
                    annot.add(('desc-label-warn', 'Context difference between versions'),
                              label=True)
        else:
            mark1 = '+'
            annot.add(('desc-label-err', 'New commit'), label=True)
        marks.append(mark1)

    def get_compared_titles(self):
        return ('v{}'.format(self.commit_getter.number),
                'v{}'.format(self.commit_getter_prev.number))

    def add_more(self, app, mr, content_defs):
        popup = app.wait_log_popup('Checking for removed commits...')

        removed = {}
        last_index = 0
        for prev_i, (prev_c, _) in enumerate(self.commit_getter_prev.get_commits(popup.append)):
            if self.prev_used[prev_i]:
                last_index = self.prev_used[prev_i]
                continue
            if last_index not in removed:
                removed[last_index] = []
            removed[last_index].append(prev_c)

        for i in sorted(removed.keys(), reverse=True):
            for c in reversed(removed[i]):
                annot = formatters.AnnotationFormatter('desc')
                annot.add(('desc-label-err', 'Removed commit'), label=True)
                diff = self.repo.get_parsed_diff(str(c.oid))
                cf = formatters.CommitFormatter(c, diff)
                disp = self.new_disp_commit(cf.name())
                disp.set_type_removed(c)
                disp.build_widgets(content_defs,
                                   annot=annot.format(), header=cf.format_header(),
                                   stat=cf.get_stat(), diff=cf.format_diff())
                disp.set_marks(['-'])
                self.insert(i + 1, disp)

        popup.stop()
