from modules import compat


class UnknownAction(Exception):
    pass


display_keys = { 'up': '↑', 'down': '↓', 'left': '←', 'right': '→',
                 'ctrl up': 'ctrl↑', 'ctrl down': 'ctrl↓',
                 'ctrl left': 'ctrl←', 'ctrl right': 'ctrl→',
                 ' ': 'space', '<0>': 'ctrl space',
               }

# translation of keys that cannot be saved in the config file to something
# that can be
config_keys = { ' ': 'space' }


action_aliases = {
    'open': 'activate',
}


# Actions that are always available for every widget:
basic_actions = ['cursor_up', 'cursor_down', 'cursor_left', 'cursor_right',
                 'cursor_max_left', 'cursor_max_right', 'cursor_page_up',
                 'cursor_page_down', 'cursor_top', 'cursor_bottom',
                 'cursor_word_left', 'cursor_word_right', 'redraw_screen',
                 'activate',
                ]


class Keys:
    # merge request
    approve = 'A'
    assign = '+'
    block = 'B'
    bug_tracker = 'b'
    ci_status = '?'
    comment = 'c'
    compare_versions = 'v'
    compare_last_approved_version = 'V'
    diff_next = 'd'
    diff_prev = 'D'
    file_filter = 'L'
    file_owners_filter = 'l'
    from_content = 'enter'
    full_activity = 'a'
    fullscreen = 'f'
    gitlab = 'g'
    hide_comments = 'h'
    hide_description = 'ctrl e'
    hide_diffstat = 'ctrl d'
    hide_series = 's'
    interdiff = 'u'
    new_thread = 'C'
    search = '/'
    search_next = 'n'
    submit_comments = 'C'
    to_commits = ('esc', 'q')
    to_content = 'enter'
    todo_add = 't'
    unapprove = 'X'
    unassign = '-'
    unblock = 'U'

    # merge request comments
    insert_template = 'ctrl t'

    # todo and other MR lists
    all_mrs = 'a'
    mark_done = 'D'
    clear_completed = 'C'
    refresh = 'r'

    # config
    back = ('esc', 'q')
    add = 'n'
    remove = 'delete'

    # general
    focus_next = 'tab'
    focus_prev = 'shift tab'
    activate = 'enter'
    next_item = ('ctrl down', 'j')
    prev_item = ('ctrl up', 'k')
    help = ('f1', 'meta h')
    quit = 'q'

    # cursor movement
    cursor_up = 'up'
    cursor_down = 'down'
    cursor_left = 'left'
    cursor_right = 'right'
    cursor_max_left = 'home'
    cursor_max_right = 'end'
    cursor_page_up = 'page up'
    cursor_page_down = ('page down', ' ')
    cursor_top = 'ctrl home'
    cursor_bottom = 'ctrl end'
    cursor_word_left = 'ctrl left'
    cursor_word_right = 'ctrl right'
    redraw_screen = 'ctrl l'

    # editor
    delete_line = 'ctrl d'
    paste = 'ctrl v'
    undo = 'ctrl z'
    redo = 'ctrl r'
    selection_start = '<0>' # ctrl space
    selection_cancel = 'esc'
    selection_copy = 'enter'

    # checkbox
    toggle = ('enter', ' ')

    def __init__(self, config):
        self._modified = set()
        if not config:
            return
        rev_config_keys = { v: k for k, v in config_keys.items() }
        for name, key in config.items():
            attr_name = name.replace('-', '_')
            attr_name = compat.compat_actions.get(attr_name, attr_name)
            if attr_name is None:
                # ignore obsoleted keys
                continue
            try:
                getattr(self, attr_name)
            except AttributeError:
                raise UnknownAction('Unknown action \'{}\' in the config file.'.format(name))
            value = tuple(rev_config_keys.get(v, v) for v in key)
            setattr(self, attr_name, value)
            self._modified.add(attr_name.replace('_', '-'))

    def __getitem__(self, name):
        name = name.replace('-', '_')
        key = getattr(self, action_aliases.get(name, name))
        if type(key) == str:
            return (key,)
        return key

    def __setitem__(self, name, value):
        attr_name = name.replace('-', '_')
        if name in self._modified:
            key = getattr(self, attr_name)
            if type(key) == str:
                key = (key,)
            if value in key:
                return
        else:
            key = ()
            self._modified.add(name)
        new_value = key + (value,)
        if len(new_value) == 1:
            new_value = new_value[0]
        setattr(self, attr_name, new_value)

    def __delitem__(self, name):
        attr_name = name.replace('-', '_')
        try:
            self._modified.remove(name)
        except KeyError:
            return
        setattr(self, attr_name, getattr(Keys, attr_name))

    def __iter__(self):
        for key in dir(self):
            if key.startswith('_'):
                continue
            yield key.replace('_', '-')


basic_actions = [a.replace('_', '-') for a in basic_actions]
