from tuimatic.text import SelectableIcon
from tuimatic.command_map import ACTIVATE

class CheckBox(SelectableIcon):
    """
    A checkbox widget suitable for use in TextBox. Unlike the original
    urwid's CheckBox, supports markup in the state rendering.
    """
    states = {
        True: '[X] ',
        False: '[ ] ',
        'mixed': '[#] '
    }

    def __init__(self, label, state=False, has_mixed=False, states=None,
                 cursor_position=None, reverse=False, **kwargs):
        """
        :param label: markup for check box label
        :param state: False, True or "mixed"
        :param has_mixed: True if "mixed" is a state to cycle through
        :param states: a dictionary of state to markup mapping
        :param reverse: switch True and False rendering
        Additionally, all parameters accepted by Text() (align, wrap, etc.)
        are accepted.
        """
        if isinstance(label, list):
            self._label = label
        else:
            self._label = [label]
        self._state = state
        self.has_mixed = has_mixed
        if states:
            self.states = states
        if reverse:
            self.states = self.states.copy()
            self.states[True], self.states[False] = self.states[False], self.states[True]
        if not cursor_position:
            cursor_position = (1, 0)
        super().__init__(self._build_text(), cursor_position=cursor_position, **kwargs)

    def _build_text(self):
        return [self.states[self._state]] + self._label

    def set_label(self, label):
        self._label = label
        self.set_text(self._build_text())

    def get_label(self):
        return self._label

    label = property(lambda self: self.get_label(),
                     lambda self, value: self.set_label(value))

    def set_state(self, state):
        if self._state == state:
            return
        if state not in self.states:
            raise KeyError('{!r} Invalid state: {!r}'.format(self, state))

        self._state = state
        self.set_text(self._build_text())

    def get_state(self):
        return self._state

    state = property(lambda self: self.get_state(),
                     lambda self, value: self.set_state(value))

    def toggle_state(self):
        if self.state == False:
            self.state = True
        elif self.state == True:
            self.state = 'mixed' if self.has_mixed else False
        elif self.state == 'mixed':
            self.state = False

    def keypress(self, size, key):
        if self._command_map[key] != ACTIVATE:
            return key
        self.toggle_state()


class RadioButton(CheckBox):
    states = {
        True: '(X) ',
        False: '( ) ',
        'mixed': '(#) '
    }

    def __init__(self, group, label, state='first True', states=None,
                 cursor_position=None, **kwargs):
        """
        :param group: list for radio buttons in same group
        :param label: markup for radio button label
        :param state: False, True, "mixed" or "first True"
        :param states: a dictionary of state to markup mapping
        Additionally, all parameters accepted by Text() (align, wrap, etc.)
        are accepted.

        This function will append the new radio button to group.
        "first True" will set to True if group is empty.
        """
        if state == 'first True':
            state = not group

        self.group = group
        super().__init__(label, state, states=states, cursor_position=cursor_position,
                         **kwargs)
        group.append(self)

    def set_state(self, state):
        if self._state == state:
            return
        super().set_state(state)

        # if we're clearing the state we don't have to worry about
        # other buttons in the button group
        if state is not True:
            return

        # clear the state of each other radio button
        for cb in self.group:
            if cb is not self:
                cb.state = False

    def toggle_state(self):
        self.state = True
