# Tuimatic Window-Icon-Menu-Pointer-style widget classes
#    Copyright (C) 2004-2011  Ian Ward
#
#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from tuimatic.widget import (WidgetWrap, delegate_to_widget_mixin, BOX, FLOW)
from tuimatic.text import (Text, SelectableIcon)
from tuimatic.canvas import CompositeCanvas
from tuimatic.signals import connect_signal
from tuimatic.container import Columns, Overlay
from tuimatic.util import is_mouse_press
from tuimatic.signals import disconnect_signal # doctests
from tuimatic.decoration import WidgetDecoration
from tuimatic.command_map import ACTIVATE


class Button(WidgetWrap):
    def sizing(self):
        return frozenset([FLOW])

    button_left = Text("<")
    button_right = Text(">")

    signals = ["click"]

    def __init__(self, label):
        """
        :param label: markup for button label

        Signals supported: ``'click'``

        Register signal handler with::

          tuimatic.connect_signal(button, 'click', callback, user_data)

        where callback is callback(button [,user_data])
        Unregister signal handlers with::

          tuimatic.disconnect_signal(button, 'click', callback, user_data)

        >>> Button(u"Ok")
        <Button selectable flow widget 'Ok'>
        >>> b = Button("Cancel")
        >>> b.render((15,), focus=True).text # ... = b in Python 3
        [...'< Cancel      >']
        """
        self._label = SelectableIcon("", 0)
        cols = Columns([
            ('fixed', 1, self.button_left),
            self._label,
            ('fixed', 1, self.button_right)],
            dividechars=1)
        self.__super.__init__(cols)

        self.set_label(label)

    def _repr_words(self):
        # include button.label in repr(button)
        return self.__super._repr_words() + [
            repr(self.label)]

    def set_label(self, label):
        """
        Change the button label.

        label -- markup for button label

        >>> b = Button("Ok")
        >>> b.set_label(u"Yup yup")
        >>> b
        <Button selectable flow widget 'Yup yup'>
        """
        self._label.set_text(label)

    def get_label(self):
        """
        Return label text.

        >>> b = Button(u"Ok")
        >>> print(b.get_label())
        Ok
        >>> print(b.label)
        Ok
        """
        return self._label.text
    label = property(get_label)

    def keypress(self, size, key):
        """
        Send 'click' signal on 'activate' command.

        >>> assert Button._command_map[' '] == 'activate'
        >>> assert Button._command_map['enter'] == 'activate'
        >>> size = (15,)
        >>> b = Button(u"Cancel")
        >>> clicked_buttons = []
        >>> def handle_click(button):
        ...     clicked_buttons.append(button.label)
        >>> key = connect_signal(b, 'click', handle_click)
        >>> b.keypress(size, 'enter')
        >>> b.keypress(size, ' ')
        >>> clicked_buttons # ... = u in Python 2
        [...'Cancel', ...'Cancel']
        """
        if self._command_map[key] != ACTIVATE:
            return key

        self._emit('click')

    def mouse_event(self, size, event, button, x, y, focus):
        """
        Send 'click' signal on button 1 press.

        >>> size = (15,)
        >>> b = Button(u"Ok")
        >>> clicked_buttons = []
        >>> def handle_click(button):
        ...     clicked_buttons.append(button.label)
        >>> key = connect_signal(b, 'click', handle_click)
        >>> b.mouse_event(size, 'mouse press', 1, 4, 0, True)
        True
        >>> b.mouse_event(size, 'mouse press', 2, 4, 0, True) # ignored
        False
        >>> clicked_buttons # ... = u in Python 2
        [...'Ok']
        """
        if button != 1 or not is_mouse_press(event):
            return False

        self._emit('click')
        return True


class PopUpLauncher(delegate_to_widget_mixin('_original_widget'),
        WidgetDecoration):
    def __init__(self, original_widget):
        self.__super.__init__(original_widget)
        self._pop_up_widget = None

    def create_pop_up(self):
        """
        Subclass must override this method and return a widget
        to be used for the pop-up.  This method is called once each time
        the pop-up is opened.
        """
        raise NotImplementedError("Subclass must override this method")

    def get_pop_up_parameters(self):
        """
        Subclass must override this method and have it return a dict, eg:

        {'left':0, 'top':1, 'overlay_width':30, 'overlay_height':4}

        This method is called each time this widget is rendered.
        """
        raise NotImplementedError("Subclass must override this method")

    def open_pop_up(self):
        self._pop_up_widget = self.create_pop_up()
        self._invalidate()

    def close_pop_up(self):
        self._pop_up_widget = None
        self._invalidate()

    def render(self, size, focus=False):
        canv = self.__super.render(size, focus)
        if self._pop_up_widget:
            canv = CompositeCanvas(canv)
            canv.set_pop_up(self._pop_up_widget, **self.get_pop_up_parameters())
        return canv


class PopUpTarget(WidgetDecoration):
    # FIXME: this whole class is a terrible hack and must be fixed
    # when layout and rendering are separated
    _sizing = set([BOX])
    _selectable = True

    def __init__(self, original_widget):
        self.__super.__init__(original_widget)
        self._pop_up = None
        self._current_widget = self._original_widget

    def _update_overlay(self, size, focus):
        canv = self._original_widget.render(size, focus=focus)
        self._cache_original_canvas = canv # imperfect performance hack
        pop_up = canv.get_pop_up()
        if pop_up:
            left, top, (
                w, overlay_width, overlay_height) = pop_up
            if self._pop_up != w:
                self._pop_up = w
                self._current_widget = Overlay(w, self._original_widget,
                    ('fixed left', left), overlay_width,
                    ('fixed top', top), overlay_height)
            else:
                self._current_widget.set_overlay_parameters(
                    ('fixed left', left), overlay_width,
                    ('fixed top', top), overlay_height)
        else:
            self._pop_up = None
            self._current_widget = self._original_widget

    def render(self, size, focus=False):
        self._update_overlay(size, focus)
        return self._current_widget.render(size, focus=focus)
    def get_cursor_coords(self, size):
        self._update_overlay(size, True)
        return self._current_widget.get_cursor_coords(size)
    def get_pref_col(self, size):
        self._update_overlay(size, True)
        return self._current_widget.get_pref_col(size)
    def keypress(self, size, key):
        self._update_overlay(size, True)
        return self._current_widget.keypress(size, key)
    def move_cursor_to_coords(self, size, x, y):
        self._update_overlay(size, True)
        return self._current_widget.move_cursor_to_coords(size, x, y)
    def mouse_event(self, size, event, button, x, y, focus):
        self._update_overlay(size, focus)
        return self._current_widget.mouse_event(size, event, button, x, y, focus)
    def pack(self, size=None, focus=False):
        self._update_overlay(size, focus)
        return self._current_widget.pack(size)






def _test():
    import doctest
    doctest.testmod()

if __name__=='__main__':
    _test()
